﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using NuGet.Protocol;
using System.Runtime.Intrinsics.X86;
using System.Text;
using web.Models;

namespace web.Controllers
{
    public class BranchesController : Controller
    {
        Uri baseAddress = new Uri("https://localhost:7299/api/");
        private readonly HttpClient _httpClient;
        public BranchesController()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = baseAddress;
        }
        [HttpGet]
        public IActionResult Index()
        {
            List<BranchViewModel> branchList = new List<BranchViewModel>();
            HttpResponseMessage response = _httpClient.GetAsync(_httpClient.BaseAddress+"Branches/GetBranches").Result;
            if (response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                branchList = JsonConvert.DeserializeObject<List<BranchViewModel>>(data);
            }
            return View(branchList);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create(BranchViewModel model)
        {
            try
            {
                string data = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(data, Encoding.UTF8,"application/json");

                HttpResponseMessage response = _httpClient.PostAsync(_httpClient.BaseAddress + "Branches/PostBranch", content).Result;

                if (response.IsSuccessStatusCode)
                {
                    TempData["successMessage"] = "Branch Created.";
                    return RedirectToAction("Index");
                }
                else if (response.StatusCode == System.Net.HttpStatusCode.Conflict)
                {
                    TempData["duplicateMessage"] = "BranchId already exists.";
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }
            return View();
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            try
            {
                BranchViewModel branch = new BranchViewModel();
                HttpResponseMessage response = _httpClient.GetAsync(_httpClient.BaseAddress + "Branches/GetBranch/" + id).Result;

                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    branch = JsonConvert.DeserializeObject<BranchViewModel>(data);
                }
                return View(branch);
            }
            catch(Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }
        }

        [HttpPost]
        public IActionResult Edit(BranchViewModel model)
        {
            try
            {
                string data = JsonConvert.SerializeObject(model);
                StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                HttpResponseMessage response = _httpClient.PutAsync(_httpClient.BaseAddress + "Branches/PutBranch/" + model.BranchId, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    TempData["successMessage"] = "Branch is updated";
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }
            return View();
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            try
            {
                BranchViewModel branch = new BranchViewModel();
                HttpResponseMessage response = _httpClient.GetAsync(_httpClient.BaseAddress + "Branches/GetBranch/" + id).Result;

                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    branch = JsonConvert.DeserializeObject<BranchViewModel>(data);
                }
                return View(branch);
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            try
            {
                HttpResponseMessage response = _httpClient.DeleteAsync(_httpClient.BaseAddress + "Branches/DeleteBranch/" + id).Result;

                if (response.IsSuccessStatusCode)
                {
                    TempData["successMessage"] = "Branch is deleted.";
                    return RedirectToAction("Index");
                }
            }
            catch ( Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }
            return View();
        }

        [HttpGet]
        public IActionResult Details (int id)
        {
            try
            {
                BranchViewModel branch = new BranchViewModel();
                HttpResponseMessage response = _httpClient.GetAsync(_httpClient.BaseAddress + "Branches/GetBranch/" + id).Result;

                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    branch = JsonConvert.DeserializeObject<BranchViewModel>(data);
                }
                return View(branch);
            }
            catch (Exception ex)
            {
                TempData["errorMessage"] = ex.Message;
                return View();
            }
        }
    }

}
