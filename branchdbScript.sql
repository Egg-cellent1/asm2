USE [master]
GO
/****** Object:  Database [BranchDB]    Script Date: 9/20/2023 12:19:42 PM ******/
CREATE DATABASE [BranchDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'BranchDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\BranchDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BranchDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\BranchDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [BranchDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [BranchDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [BranchDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [BranchDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [BranchDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [BranchDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [BranchDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [BranchDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [BranchDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [BranchDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [BranchDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [BranchDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [BranchDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [BranchDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [BranchDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [BranchDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [BranchDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [BranchDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [BranchDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [BranchDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [BranchDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [BranchDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [BranchDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [BranchDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [BranchDB] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [BranchDB] SET  MULTI_USER 
GO
ALTER DATABASE [BranchDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [BranchDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [BranchDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [BranchDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [BranchDB] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [BranchDB] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [BranchDB] SET QUERY_STORE = OFF
GO
USE [BranchDB]
GO
/****** Object:  Table [dbo].[Branch]    Script Date: 9/20/2023 12:19:42 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Branch](
	[BranchId] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Address] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[State] [nvarchar](50) NULL,
	[ZipCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_Branch] PRIMARY KEY CLUSTERED 
(
	[BranchId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (1, N'Ohio', N'USA', N'lorem', N'Active', N'10809')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (2, N'Dalat', N'VN', N'ipsum', N'Deactive', N'44033')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (3, N'Texas', N'USA', N'dream', N'Active', N'10002')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (4, N'Michigan', N'USA', N'bruh', N'Active', N'18763')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (5, N'NYC', N'USA', N'New York', N'Active', N'99897')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (6, N'Alabama', N'USA', N'ThanhPho', N'Deactive', N'56311')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (10, N'aaaaa2', N'a', N'a', N'Deactive', N'1')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (12, N'aaaaa', N'a', N'a', N'Deactive', N'1')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (13, N'aaaaa', N'a', N'a', N'Active', N'1')
INSERT [dbo].[Branch] ([BranchId], [Name], [Address], [City], [State], [ZipCode]) VALUES (14, N'aaaaa', N'a', N'a', N'Active', N'1')
GO
USE [master]
GO
ALTER DATABASE [BranchDB] SET  READ_WRITE 
GO
